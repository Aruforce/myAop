package com.aruforce.myAop.jvmagent;

import java.lang.instrument.Instrumentation;

/**
 * @Author
 * JVM 提供了一种JVM加载后(在main方法之前)执行agentJat内premain方法的机制
 * 启动一个pre-Main-Agent的方式是使用command-line 参数指定Jar的path：-javaagent:pathToAgentJar[agentArgs];
 * 注意可以有多个-javaagent参数sample：
 * java -jar HelloWorld.jar -javaagent:pathToAgentA[agentArgs] -javaagent:pathToAgentB[agentArgs]
 */
public class Agent {

    public static Instrumentation instrumentation = null;
    public static String agentArgs = null;

    /**
     *  JVM初始化完成后,会按照命令行的指定Agent的顺序依次调用每个agentJar包内的Premain-class 值指定类的premain方法，全部执行完成后后再执行main方法;
     *  JVM会优先尝试执行{@link #premain(String agentArgs,Instrumentation instrument)}，如果成功则执行下一个Agent，否则JVM退出
     * @param agentArgs
     * @param instrument
     */
    public static void premain(String agentArgs,Instrumentation instrument){
        System.out.println("now invoking method 'premain(String agentArgs,Instrumentation instrument)'");
        Agent.agentArgs = agentArgs;
        Agent.instrumentation = instrument;
        CustomClassTransformer customClassTransformer1 = new CustomClassTransformer(); customClassTransformer1.tag ="trans1";
        CustomClassTransformer customClassTransformer2 = new CustomClassTransformer(); customClassTransformer2.tag ="trans2";
        instrumentation.addTransformer(customClassTransformer2,false);
        instrumentation.addTransformer(customClassTransformer1,true);
    }
    /**
     * 在JVM 运行时
     * @param agentArgs
     * @param instrumentation
     */
    public static void agentmain(String agentArgs,Instrumentation instrumentation){
        System.out.println("now invoking method 'agentmain(String agentArgs,Instrumentation instrument)'");
        Agent.agentArgs = agentArgs;
        Agent.instrumentation = instrumentation;
        CustomClassTransformer customClassTransformer1 = new CustomClassTransformer(); customClassTransformer1.tag ="trans1";
        CustomClassTransformer customClassTransformer2 = new CustomClassTransformer(); customClassTransformer2.tag ="trans2";
        instrumentation.addTransformer(customClassTransformer2,false);
        instrumentation.addTransformer(customClassTransformer1,true);
    }
}
