package com.aruforce.myAop.jvmagent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class CustomClassTransformer implements ClassFileTransformer {
    public String tag ;
    private static final String doChangeClassName = "";
    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        System.out.println("CustomClassTransformer"+tag+"now process ["+className+"]");
        return null;
    }
}
