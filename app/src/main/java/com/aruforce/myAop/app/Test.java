package com.aruforce.myAop.app;

import com.aruforce.myAop.jvmagent.Agent;
import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;

import java.io.IOException;
import java.lang.instrument.UnmodifiableClassException;

public class Test {
    private static final String agentPath = "D:\\WorkSpacMvn\\myAop\\jvm-agent\\target\\jvm-agent-0.0.1-SNAPSHOT.jar";
    public static void main(String[] args){
        try {
            VirtualMachineAttchTools.attechAgent(agentPath);
        } catch (AgentLoadException e) {
            e.printStackTrace();
        } catch (AgentInitializationException e) {
            e.printStackTrace();
        } catch (AttachNotSupportedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Logic.doLogic();
        try {
            Agent.instrumentation.retransformClasses(Logic.class);
        } catch (UnmodifiableClassException e) {
            e.printStackTrace();
        }
    }
}
