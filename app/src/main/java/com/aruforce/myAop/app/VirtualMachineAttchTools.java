package com.aruforce.myAop.app;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;

import java.io.IOException;
import java.lang.management.ManagementFactory;

public class VirtualMachineAttchTools {
    public static void  attechAgent(String agentPath) throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException {
        String processDs = ManagementFactory.getRuntimeMXBean().getName();
        String pid = "";
        if (processDs.indexOf("@")>0){
            pid = processDs.substring(0, processDs.indexOf("@"));
        } else{
            pid = processDs;
        }
        VirtualMachine currentVM = VirtualMachine.attach(pid);
        currentVM.loadAgent(agentPath);
        currentVM.detach();
    }
}
